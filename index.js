/* main module entry point */
var cfg = require('./lib/config')
var log = require('./lib/log')
var app = require('./lib/app')

app.listen(cfg.port)
log.info('app listening on port', cfg.port)
